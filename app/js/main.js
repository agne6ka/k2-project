/**
 * Created by aga on 23.04.16.
 */

(function() {
    var buttons = document.getElementsByClassName("toggle-speaker");
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', function (e) {
            var parent = findAncestor(this, 'speaker-bg');
            parent.classList.toggle('active');
        }, false);
    }
    function findAncestor(el, cls) {
        while ((el = el.parentElement) && !el.classList.contains(cls));
        return el;
    }
})();
(function() {
    var btn = document.getElementsByClassName("btn"), navi = document.getElementById('navi');
    var addBtn = function() {
        navi.classList.add("btn-active");
        };
    var rmBtn = function(){
        navi.classList.remove("btn-active");
    };
    for (var i = 0; i < btn.length; i++) {
        btn[i].addEventListener('mouseover', addBtn, false);
        btn[i].addEventListener('mouseout', rmBtn, false);
    }
})();

$(function () {
    var height = $('#scroll-wrapper').innerHeight();
    $(window).scroll(function()
    {
        var wintop = $(window).scrollTop();
        if (wintop < height * 3){
            $('#scroll').css({"top": wintop / 3});
        }
    });
});