// Include gulp
var gulp = require('gulp');

// Requires the gulp-sass plugin
var sass = require('gulp-sass');

// Lint Task
gulp.task('sass', function(){
  return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'));
});

// Gulp watcher
gulp.task('sass:watch', function () {
  gulp.watch('app/scss/**/*.scss', ['sass']);
});